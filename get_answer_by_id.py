from readwriter import CsvReader, CsvWriter
import sys
import json

param = sys.argv
aid = int(param[1])-1

all_data = CsvReader().to_dict()

result = all_data[aid]
print(json.dumps(result, ensure_ascii=False))

