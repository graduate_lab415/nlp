import sys
import json
import csv
import os
import datetime

param = sys.argv
q = param[1]
qa_id = param[2]
category_id = param[3]
result = dict()

if qa_id == "-1":
    result['new_question'] = True
    answer = ""
    remark = "New Question"
else:
    result['new_question'] = False
    qa = os.popen(
        "/home/yr/PycharmProjects/nlp/venv/bin/python3 /home/yr/PycharmProjects/nlp/get_answer_by_id.py " + qa_id).read()
    qa = json.loads(qa)
    answer = qa['A']
    remark = "Origin Question "+qa['Q']

file_exist = os.path.isfile("/home/yr/PycharmProjects/nlp/output/adjustments.csv")
csv_file = open("/home/yr/PycharmProjects/nlp/output/adjustments.csv", "a+")
writer = csv.writer(csv_file, delimiter=',')

'''Add CSV Title'''
if not file_exist:
    csv_file = open("/home/yr/PycharmProjects/nlp/output/adjustments.csv", "w")
    writer = csv.writer(csv_file, delimiter=',')
    writer.writerow(['id', 'Intent ID', 'Q', 'A', 'category', 'source', 'time', 'remark'])

'''Add Row'''
csv_file = open("/home/yr/PycharmProjects/nlp/output/adjustments.csv", "a+")
writer = csv.writer(csv_file, delimiter=',')
new_row = ['', '', q, answer, category_id, 'User Input', str(datetime.datetime.now()), remark]

success = writer.writerow(new_row)

if success > 6:
    '''
    new_row = ['', '', '', '', '']
    because len(new_raw) is 6
    '''
    result['success'] = 1
else:
    result['success'] = -1

print(json.dumps(result, ensure_ascii=False))
