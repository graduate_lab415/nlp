import calendar
import time


def write_to_file(filename, message):
    # 開啟檔案
    fp = open("/home/yr/PycharmProjects/nlp/output/%s_%d.json" % (filename, calendar.timegm(time.gmtime())), "w")

    # 寫入 This is a testing! 到檔案
    fp.write(message)

    # 關閉檔案
    fp.close()
