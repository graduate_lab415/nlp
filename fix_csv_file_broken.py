import csv
from readwriter import CsvWriter


def find_row_by_url(url):
    file = open("docs/問題整理_20201116.csv", "r", encoding='mac_romanian', newline='')
    data_broken = csv.DictReader(file)
    if url == "":
        return 0

    for row_broken in data_broken:
        if url == row_broken['url']:
            # print("%s = %s" % (url, row_broken['url']))
            return row_broken["category"]
    file.close()
    return 0


file_bk = open("docs/問題整理_20201116.csv.bk", "r", newline='')
data_old = csv.DictReader(file_bk)

csv_writer = CsvWriter("fixed_file")
csv_writer.write_head(["id", "Intent ID", "Q", "A", "url", "category"])

data_fixed = []
for i, row_old in enumerate(data_old):
    category = find_row_by_url(row_old['出處'])
    row_old["category"] = category
    csv_writer.write_row([row_old['編號'], row_old['Intent ID'], row_old['Q'], row_old['A'], row_old['出處'], row_old['category']])
    print(row_old, category)

