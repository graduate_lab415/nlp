import csv
import calendar
import time


class CsvReader:
    def __init__(self):
        file = open("/home/yr/PycharmProjects/nlp/docs/fixed_file_1622789141_remove_duplicate_labeled_renumber.csv",
                    newline='')
        # 讀取 CSV 檔內容，將每一列轉成一個 dictionary
        self.data = csv.DictReader(file)

    def get_data(self):
        return self.data

    def get_field(self, field_name):
        field_data = []
        field_data_category = {}
        for row in self.data:
            if row[field_name] != "":
                category = row["category"]
                content_of_field = row[field_name]

                field_data.append(content_of_field)
                if category in field_data_category:
                    field_data_category[category].append(content_of_field)
                else:
                    field_data_category[category] = [content_of_field]
        return field_data, field_data_category

    def to_dict(self):
        rows = []
        rows_category = {}
        for row in self.data:
            if row['Q'] != "":
                rows.append({'id': row['id'],
                             'Q': row['Q'],
                             'A': row['A'],
                             'url': row['url'],
                             'category': row['category']})
                if row['category'] in rows_category:
                    rows_category[row['category']].append({'id': row['id'],
                                                           'Q': row['Q'],
                                                           'A': row['A'],
                                                           'url': row['url'],
                                                           'category': row['category']})
                else:
                    category_list = [{'id': row['id'],
                                      'Q': row['Q'],
                                      'A': row['A'],
                                      'url': row['url'],
                                      'category': row['category']}]
                    rows_category[row['category']] = category_list
        return rows, rows_category


class CsvWriter:
    def __init__(self, filename):
        self.file = open("/home/yr/PycharmProjects/nlp/output/%s_%d.csv" % (filename, calendar.timegm(time.gmtime())),
                         'w', newline='')
        self.writer = csv.writer(self.file, delimiter=',')

    def write_head(self, title):
        # print(title)
        self.writer.writerow(title)

    def write_row(self, row):
        self.writer.writerow(row)

    def write_rows(self, table):
        self.writer.writerows(table)
