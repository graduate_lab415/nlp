import jieba
from ckiptagger import WS


def cut_jeiba(sentence, log=False):
    words = list(jieba.cut(sentence, cut_all=False))
    if log:
        print("Input：", sentence)
        print("Output：", words)
    return words


def cut_ckip(sentence_list, log=False):
    ws = WS("/home/yr/DATA/ckip_data")
    word_s = ws(sentence_list,
                sentence_segmentation=True,
                segment_delimiter_set={'?', '？', '!', '！', '。', ',',
                                       '，', ';', ':', '、', '(', ')',
                                       '（', '）', '「', '」', '：', '_',
                                       '“', '”', '→'})
    del ws  # 釋放記憶體
    if log:
        i = 0
        while i < len(sentence_list):
            print("Input：", sentence_list[i])
            print("Output", word_s[i])
            i += 1
    return word_s
