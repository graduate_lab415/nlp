"""
參考 https://github.com/MorvanZhou/NLP-Tutorials
"""
import re
import sys
import json

import numpy as np
from collections import Counter
import itertools
from visual import show_tfidf, heatmap
from readwriter import CsvReader, CsvWriter
import CutSentence
from filewriter import write_to_file

np.seterr(divide='ignore', invalid='ignore')

"""print np without truncation"""
np.set_printoptions(threshold=sys.maxsize)

"""
CUT_METHOD = test | ckip | jieba
"""
CUT_METHOD = 'ckip'
"""
LOG = True | False
default: False
"""
LOG = False
"""
LABEL = True | False
default: True
"""
LABEL = True

"""Source"""
q_category = sys.argv[2]
if CUT_METHOD in ["ckip", "jieba"]:
    docs, docs_category = CsvReader().get_field("Q")
    all_data, all_data_category = CsvReader().to_dict()

    """group qa by category and write json to file"""
    # write_to_file("qa_group_by_category", json.dumps(all_data_category, ensure_ascii=False))

    if q_category in ['1', '2', '3', '4', '5']:
        docs = docs_category[q_category]
        all_data = all_data_category[q_category]

else:
    docs = [
        "it is a good day, I like to stay here",
        "I am happy to be here",
        "I am bob",
        "it is sunny today",
        "I have a party today",
        "it is a dog and that is a cat",
        "there are dog and cat on the tree",
        "I study hard this morning",
        "today is a good day",
        "tomorrow will be a good day",
        "I like coffee, I like book and I like apple",
        "I do not like it",
        "I am kitty, I like bob",
        "I do not care who like bob, but I like kitty",
        "It is coffee time, bring your cup",
    ]
    all_data = docs

"""斷詞"""
if CUT_METHOD == "jieba":
    docs_words = [CutSentence.cut_jeiba(re.sub('[?？()（）「」,，、。:：_“”→]', ' ', d), log=LOG) for d in docs]  # use jieba
elif CUT_METHOD == "ckip":
    docs_words = CutSentence.cut_ckip(docs, log=LOG)  # use ckip
else:
    docs_words = [d.replace(",", "").split(" ") for d in docs]


def add_label(doc,
              label_config=json.load(open("/home/yr/PycharmProjects/nlp/docs/entities_config.json", "r"))):
    """labeling"""
    doc_tmp = list()
    for word in doc:
        word_tmp = word
        for entity_name in label_config:
            if word in label_config[entity_name]:
                word_tmp = entity_name
        doc_tmp.append(word_tmp)
    docs_words_labeled.append(doc_tmp)
    if LOG:
        print("before labeling:\t{}".format(doc))
        print("after labeling:\t\t{}".format(doc_tmp))
    return doc_tmp


"""use labeled or unlabeled"""
if LABEL:
    label_conf = json.load(open("/home/yr/PycharmProjects/nlp/docs/entities_config.json", "r"))
    docs_words_labeled = list()
    for doc_words in docs_words:
        doc_labeled = add_label(doc_words, label_conf)
    docs_words = docs_words_labeled

"""
將詞作成 set
v2i = {'word': index}
i2v = {index: 'word'}
"""
vocab = set(itertools.chain(*docs_words))
v2i = {v: i for i, v in enumerate(vocab)}
i2v = {i: v for v, i in v2i.items()}


def safe_log(x):
    mask = x != 0
    x[mask] = np.log(x[mask])
    return x


tf_methods = {
    "log": lambda x: np.log(1 + x),
    "augmented": lambda x: 0.5 + 0.5 * x / np.max(x, axis=1, keepdims=True),
    "boolean": lambda x: np.minimum(x, 1),
    "log_avg": lambda x: (1 + safe_log(x)) / (1 + safe_log(np.mean(x, axis=1, keepdims=True))),
}
idf_methods = {
    "log": lambda x: 1 + np.log(len(docs) / (x + 1)),
    "prob": lambda x: np.maximum(0, np.log((len(docs) - x) / (x + 1))),
    "len_norm": lambda x: x / (np.sum(np.square(x)) + 1),
}


def get_tf(method="log"):
    """term frequency: how frequent a word appears in a doc"""
    _tf = np.zeros((len(vocab), len(docs)), dtype=np.float64)  # [n_vocab, n_doc]
    for i, d in enumerate(docs_words):
        counter = Counter(d)
        for v in counter.keys():
            _tf[v2i[v], i] = counter[v] / counter.most_common(1)[0][1]

    weighted_tf = tf_methods.get(method, None)
    if weighted_tf is None:
        raise ValueError

    return weighted_tf(_tf)


def get_idf(method="log"):
    """df: document frequency"""
    """inverse document frequency: low idf for a word appears in more docs, mean less important"""
    df = np.zeros((len(i2v), 1))  # [n_vocab, 1]
    for i in range(len(i2v)):
        d_count = 0
        for d in docs_words:
            d_count += 1 if i2v[i] in d else 0
        df[i, 0] = d_count
    if LOG:
        print("\ndf samples:\n", df)
        """print df to a csv file"""
        csv_writer = CsvWriter("df")
        csv_writer.write_head(list(v2i))
        csv_writer.write_row(df)

    idf_fn = idf_methods.get(method, None)
    if idf_fn is None:
        raise ValueError
    return idf_fn(df)


def cosine_similarity(query, _tf_idf):
    """
    餘弦相似度無關乎向量大小，重點是向量之間的方向。
    方向一致時餘弦值為 1
    直角時餘弦相似度的值為 0
    方向相反時餘弦值為 -1
    """
    unit_q = query / np.sqrt(np.sum(np.square(query), axis=0, keepdims=True))
    unit_ds = _tf_idf / np.sqrt(np.sum(np.square(_tf_idf), axis=0, keepdims=True))
    similarity = unit_ds.T.dot(unit_q).ravel()
    return similarity


def docs_score(query, len_norm=False):
    """斷詞"""
    if CUT_METHOD == "jieba":
        q_words = CutSentence.cut_jeiba(re.sub('[?？()（）「」,，、。:：_“”→]', ' ', query), log=LOG)  # use jieba
    elif CUT_METHOD == "ckip":
        q_words = CutSentence.cut_ckip(query, log=LOG)  # use ckip
        temp_arr = np.array(q_words)
        temp_arr = temp_arr.reshape(len(q_words[0]))
        q_words = temp_arr
    else:
        q_words = query.replace(",", "").split(" ")

    """label"""
    if LABEL:
        q_words = add_label(q_words)

    """add unknown words"""
    unknown_v = 0
    for v in set(q_words):
        if v not in v2i:
            v2i[v] = len(v2i)
            i2v[len(v2i) - 1] = v
            unknown_v += 1
    if unknown_v > 0:
        _idf = np.concatenate((idf, np.zeros((unknown_v, 1), dtype=np.float)), axis=0)
        _tf_idf = np.concatenate((tf_idf, np.zeros((unknown_v, tf_idf.shape[1]), dtype=np.float)), axis=0)
    else:
        _idf, _tf_idf = idf, tf_idf
    counter = Counter(q_words)
    q_tf = np.zeros((len(_idf), 1), dtype=np.float)  # [n_vocab, 1]
    for v in counter.keys():
        q_tf[v2i[v], 0] = counter[v]  # 為什麼不用除以 / counter.most_common(1)[0][1]
    q_vec = q_tf * _idf  # [n_vocab, 1]

    q_scores = cosine_similarity(q_vec, _tf_idf)  # 拿q的向量和所有向量比對
    if len_norm:
        len_docs = [len(d) for d in docs_words]
        q_scores = q_scores / np.array(len_docs)
    return q_scores


def get_keywords(n=2):
    for c in range(3):
        col = tf_idf[:, c]
        idx = np.argsort(col)[-n:]
        print("doc{}, top{} keywords {}".format(c, n, [i2v[i] for i in idx]))


tf = get_tf()  # [n_vocab, n_doc]
idf = get_idf()  # [n_vocab, 1]
# heatmap(idf, "idf")
tf_idf = tf * idf  # [n_vocab, n_doc]
if LOG:
    print("tf shape(vocab in each docs): ", tf.shape)
    print("\ntf samples:\n", tf[:2])
    print("\nidf shape(vocab in all docs): ", idf.shape)
    print("\nidf samples:\n", idf[:2])
    print("\ntf_idf shape: ", tf_idf.shape)
    print("\ntf_idf sample:\n", tf_idf[:2])
    get_keywords(20)

    # print tfidf to file
    tfidf_output_file = CsvWriter("tfidf")
    tfidf_output_file.write_head(list(v2i))
    tfidf_output_file.write_rows(tf_idf.T)

"""test"""
q = sys.argv[1]
# q = "拐杖去哪裡申請"
# q = "I like kitty"
if CUT_METHOD == "ckip":
    tmp_q = [q]
    q = tmp_q

scores = docs_score(q)
d_ids = scores.argsort()[-3:][::-1]
if LOG:
    print("\n[{}] top 3 docs for '{}':\n{}".format(CUT_METHOD, (q if CUT_METHOD != "ckip" else q[0]), [docs[i] for i in d_ids]))

"""Add score into result json"""
for i in d_ids:
    all_data[i]["score"] = str(scores[i])

result = {
    'cut_method': CUT_METHOD,
    'labeling': LABEL,
    'q': q if CUT_METHOD != "ckip" else q[0],
    'data': [all_data[i] for i in d_ids]
}
print(json.dumps(result, ensure_ascii=False))

"""list sentence by keywords"""
# all_sentences = dict()
# for v in v2i:
#     sentences = list()
#     for i, doc in enumerate(docs_words):
#         if v in doc:
#             sentences.append(docs[i])
#     all_sentences[v] = sentences
# list_sentence_by_keywords = json.dumps(all_sentences, ensure_ascii=False)
# file = open("/home/yr/PycharmProjects/nlp/output/list_sentence_by_keywords.json", 'w', newline='')
# file.write(list_sentence_by_keywords)


"""draw"""
# show_tfidf(tf_idf.T, [i2v[i] for i in range(len(i2v))], "tfidf_matrix")
