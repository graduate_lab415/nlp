# README

- 建議使用 pycharm 編輯
- 本專案使用 venv 套件

## 資料集

`docs/fixed_file_1622789141_remove_duplicate_labeled_renumber.csv` 是最完整的資料集

## 版本說明

本專案使用 git 版本控管

dev 分支： 問答集使用單一分類
Feature/multi-categories 分支： 問答集使用多個分類

## log 紀錄

使用 PHP API 呼叫本專案的程式時, log 會寫到 `output/log.txt`

## 執行

Pycharm 執行的設定檔在 `.idea/runConfigurations`
